# Hatch Fill extension for Inkscape

Extension based upon the eggbot_hatch extension from https://github.com/evil-mad/EggBot.

Creates plottable hatches (simple, cross-hatch, connected to a single path or separate) in all closed objects in the drawing or in all selected objects.

![Example hatches](hatch_fill_extension.svg)

Improvements include:

* Updated to work with Inkscape 1.0 and higher
* Simplified a small portion of the code
* Added the option to choose units for the hatch distance and inset
* Only depends on inkex now, which makes installation much easier

License: GPLv2 or higher
